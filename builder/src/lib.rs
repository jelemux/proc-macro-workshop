use proc_macro::TokenStream;
use proc_macro2::{Ident, Span, TokenStream as TokenStream2};
use quote::quote;
use syn::{parse_macro_input, DeriveInput, Data, Fields, FieldsNamed, Type, TypePath, Path, PathSegment, PathArguments, AngleBracketedGenericArguments, GenericArgument};

#[proc_macro_derive(Builder)]
pub fn derive(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as DeriveInput);
    let name = input.ident;
    let builder_name = Ident::new(&format!("{}Builder", name), Span::call_site());
    let fields = fields_from_data_unchecked(&input.data);

    let builder_struct = builder_struct(&builder_name, fields);
    let field_functions = field_functions(fields);
    let build_function = build_function(&name, fields);

    let expanded = quote! {
        use std::error::Error;

        #[derive(Debug, Default, Clone)]
        #builder_struct

        impl #builder_name {
            #field_functions
            #build_function
        }

        impl #name {
            fn builder() -> #builder_name {
                Default::default()
            }
        }
    };

    TokenStream::from(expanded)
}

fn fields_from_data_unchecked(data: &Data) -> &FieldsNamed {
    if let Data::Struct(ref data) = *data {
        if let Fields::Named(ref fields) = data.fields {
            return fields;
        }
    }
    panic!("data should be a struct and fields should be named") // TODO better error handling
}

fn builder_struct(builder_ident: &Ident, fields: &FieldsNamed) -> TokenStream2 {
    let builder_fields = fields.named.iter().map(|f| {
        let name = &f.ident;
        let field_ty = &f.ty;

        if nth_segment_of_type_ident_matches_str(0, field_ty, "Option") {
            quote! {
                #name: #field_ty
            }
        } else {
            quote! {
                #name: Option<#field_ty>
            }
        }
    });
    quote! {
        pub struct #builder_ident {
            #(#builder_fields),*
        }
    }
}

fn field_functions(fields: &FieldsNamed) -> TokenStream2 {
    let functions = fields.named.iter().map(|f| {
        let name = &f.ident;
        let field_ty = &f.ty;

        if nth_segment_of_type_ident_matches_str(0, field_ty, "Option") {
            if let Some(GenericArgument::Type(ty)) = kth_inner_arg_of_nth_segment_of_type(0, 0, field_ty) {
                quote! {
                    pub fn #name(&mut self, #name: #ty) -> &mut Self { // FIXME find out wrapped type
                        self.#name = Some(#name);
                        self
                    }
                }
            } else {
                panic!("inner type of Option should exist");
            }
        } else {
            quote! {
                pub fn #name(&mut self, #name: #field_ty) -> &mut Self {
                    self.#name = Some(#name);
                    self
                }
            }
        }
    });
    quote! {
        #(#functions)*
    }
}

fn nth_segment_of_type(n: usize, ty: &Type) -> Option<&PathSegment> {
    if let Type::Path(
        TypePath {
            qself: None,
            path: Path {
                leading_colon: None,
                segments
            },
        },
    ) = ty {
        return segments.iter().nth(n);
    }
    None
}

fn kth_inner_arg_of_nth_segment_of_type(k: usize, n: usize, ty: &Type) -> Option<&GenericArgument> {
    if let Some(
        PathSegment {
            arguments: PathArguments::AngleBracketed(
                AngleBracketedGenericArguments { 
                    args, 
                    .. 
                }
            ),
            .. 
        }
    ) = nth_segment_of_type(n, ty) {
        return args.iter().nth(k);
    } 
    None
}

fn nth_segment_of_type_ident_matches_str(n: usize, ty: &Type, matches: &str) -> bool {
    if let Some(PathSegment { ident, .. }) = nth_segment_of_type(n, ty) {
        if ident.to_string() == matches {
            return true;
        }
    }
    false
}

fn build_function(struct_ident: &Ident, fields: &FieldsNamed) -> TokenStream2 {
    let unwrap_fields = fields.named.iter().map(|f| {
        let name = &f.ident;
        let name_string = match name {
            None => panic!("name should be something"),
            Some(name) => name.to_string(),
        };

        if nth_segment_of_type_ident_matches_str(0, &f.ty, "Option") {
            quote! {
                let #name = match self.#name.clone() {
                    None => None,
                    Some(value) => value,
                };
            }
        } else {
            quote! {
                let #name = match self.#name.clone() {
                    None => return Err(Box::<dyn Error>::from(
                        format!("{} not set", #name_string)
                    )),
                    Some(value) => value, 
                };
            }
        }
    });
    let field_names = fields.named.iter().map(|f| &f.ident);
    quote! {
        pub fn build(&mut self) -> Result<#struct_ident, Box<dyn Error>> {
            #(#unwrap_fields)*

            Ok(#struct_ident {
                #(#field_names),*
            })
        }
    }
}